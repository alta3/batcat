(function () {
  "use strict";

  const codeEl = document.querySelector(
    "#output > div > table > tbody > tr > td:nth-child(2)"
  );

  const copyBtn = document.createElement("button");
  copyBtn.classList.add("copy-btn");
  tippy(copyBtn, {
    trigger: "click",
    content: "Copied!",
    hideOnClick: false,
    onShow(instance) {
      setTimeout(() => {
        instance.hide();
      }, 1000);
    },
  });

  codeEl.appendChild(copyBtn);

  const clipboard = new ClipboardJS(".copy-btn", {
    target: () => codeEl,
  });

  clipboard.on("success", function (e) {
    e.clearSelection();
  });
})();
