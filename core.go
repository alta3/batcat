package main

import (
	"bufio"
	"errors"
	"fmt"
	"html/template"
	"math"
	"net/http"
	"strconv"
	"strings"

	"github.com/alecthomas/chroma"
	"github.com/alecthomas/chroma/formatters/html"
	"github.com/alecthomas/chroma/lexers"
	"github.com/alecthomas/chroma/styles"
)

type lineRange struct {
	start int
	stop  int
}

// parseLineRange makes sense out of the ?lines query parameter
func parseLineRange(linesParam string) (lineRange, error) {
	lr := lineRange{
		start: 0,
		stop:  math.MaxInt,
	}
	if linesParam == "" {
		return lr, nil
	}

	split := strings.Split(linesParam, "-")
	if len(split) != 2 {
		return lr, errors.New("invalid lines parameter. Should be: ?lines=1-20")
	}

	start, err := strconv.Atoi(split[0])
	if err != nil {
		return lr, errors.New("invalid line start")
	}

	// Gonna say 1 means 0
	if start == 1 {
		start = 0
	}

	stop, err := strconv.Atoi(split[1])
	if err != nil || stop < start {
		return lr, errors.New("invalid line stop")
	}

	lr.start = start
	lr.stop = stop

	return lr, nil
}

// getRaw
func getRaw(path string, lr lineRange) (string, *http.Response, error) {
	host := "https://raw.githubusercontent.com"
	url := host + path
	res, err := http.Get(url)
	if err != nil {
		return res.Status, res, err
	}

	scan := bufio.NewScanner(res.Body)
	builder := new(strings.Builder)
	lineNum := 0
	for scan.Scan() {
		if lineNum >= lr.start && lineNum <= lr.stop {
			builder.WriteString(scan.Text() + "\n")
		}
		if lineNum == lr.stop {
			break
		}

		lineNum++
	}

	return builder.String(), res, nil
}

func handleHighlight(config Config) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		lr, err := parseLineRange(r.URL.Query().Get("lines"))
		if err != nil {
			badRequest(w, err.Error())
			return
		}

		raw, res, err := getRaw(r.URL.Path, lr)
		// err here is for protocol failures, not bad status code
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "%s\n", http.StatusText(http.StatusInternalServerError))
			return
		}

		// propagate any response that isn't ok
		if res.StatusCode != http.StatusOK {
			w.WriteHeader(res.StatusCode)
			fmt.Fprintf(w, res.Status)
			return
		}

		theme := r.URL.Query().Get("theme")
		if theme == "" {
			theme = config.HighlightTheme
		}

		highlighted, err := highlightContent(theme, r.URL.Path, raw, lr.start)
		if err != nil {
			serverError(w, err, "Unexpected error highlighting content")
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "%s\n", http.StatusText(http.StatusInternalServerError))
			return
		}

		writePage(w, r.URL.Path, highlighted)
	})
}

func highlightContent(theme, path, content string, start int) (template.HTML, error) {
	lexer := lexers.Match(path)
	if lexer == nil {
		lexer = lexers.Fallback
	}
	lexer = chroma.Coalesce(lexer)
	style := styles.Get(theme)
	if style == nil {
		style = styles.Fallback
	}

	// display the 0th line as 1
	if start == 0 {
		start = 1
	}

	formatter := html.New(
		html.WithLineNumbers(true),
		html.LineNumbersInTable(true),
		html.BaseLineNumber(start),
	)

	iterator, err := lexer.Tokenise(nil, content)
	if err != nil {
		return "", err
	}

	builder := new(strings.Builder)
	err = formatter.Format(builder, style, iterator)
	if err != nil {
		return "", err
	}

	return template.HTML(builder.String()), nil
}

type PageData struct {
	Title   string
	Content template.HTML
}

func writePage(w http.ResponseWriter, path string, highlight template.HTML) {
	tmp := template.Must(template.ParseFS(static, "static/template.html"))
	tmp.Execute(w, PageData{
		Title:   path,
		Content: highlight,
	})
}
