package main

import (
	"embed"
	"net/http"
)

//go:embed static
var static embed.FS

type Config struct {
	ListenSocket   string
	HighlightTheme string
}

func getConfig() Config {
	return Config{
		ListenSocket:   getEnv("BATCAT_LISTEN_SOCKET", "0.0.0.0:2885"),
		HighlightTheme: getEnv("BATCAT_HIGHLIGHT_THEME", "dracula"),
	}
}

func main() {
	config := getConfig()

	http.Handle("/static/", http.FileServer(http.FS(static)))
	http.Handle("/", handleHighlight(config))
	http.ListenAndServe(config.ListenSocket, nil)
}
