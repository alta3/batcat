# `batcat`

Does not include :bat: or :cat:

It does syntax highlight content returned from `https://raw.githubusercontent.com` though.

## Usage

```bash
# Building with go
go build .
# Or, build a docker image
make image

./batcat
# Default settings:
#  Listens on: 0.0.0.0:2885
#  Color theme: dracula (duh)

# Configure using environment variables, for example
BATCAT_LISTEN_SOCKET=0.0.0.0:8080
BATCAT_HIGHLIGHT_THEME=doom-one2

# For a given https://raw.githubusercontent.com url, such as:
https://raw.githubusercontent.com/alta3/kubernetes-the-alta3-way/main/labs/yaml/fqdn.yaml

# Make a request to this service, for example:
https://batcat.alta3.com/alta3/kubernetes-the-alta3-way/main/labs/yaml/fqdn.yaml

# Optional query parameters:
?lines=1-10
?theme=monokai
?lines=20-30&theme=doom-one
```
