package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

// getEnv attempts to look up an environment variable, defaulting to a fallback value if it is not present
func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func badRequest(w http.ResponseWriter, msg string) {
	w.WriteHeader(http.StatusBadRequest)
	fmt.Fprintf(w, "%s\n", msg)
}

func serverError(w http.ResponseWriter, err error, msg string) {
	log.Printf("ERROR: %s\n", err)
	w.WriteHeader(http.StatusInternalServerError)
	fmt.Fprintf(w, "%s\n", msg)
}
