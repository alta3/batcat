image:
	docker build -t github.com/alta3/batcat .

run:
	docker run \
		--rm \
		-it -p 2885:2885 \
		github.com/alta3/batcat